*IMPORTANT* This project was moved to github. The source here is not complete. The completed prject can be found in the github repo - https://github.com/mdilovar/rubik_n
# rubik_n

An online 3D Rubik's Cube game.
Players can choose cubes of various sizes (2x2x2 - 10x10x10) and compete with their Facebook friends.

https://rubikn.herokuapp.com

Uses three.js.
